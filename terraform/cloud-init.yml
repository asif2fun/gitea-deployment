#cloud-config
package_update: true
package_upgrade: true

packages:
  - git
  - docker.io
  - docker-compose
  - certbot
  - openssl  # To generate secure tokens
  - pass
  - gnupg

runcmd:
  # User & Docker Setup
  - usermod -aG docker AzureAdmin
  - systemctl enable docker
  - systemctl start docker
  - mkdir -p /srv/gitea
  - cd /srv/gitea
  - mkdir -p /srv/gitea/custom/conf
  - mkdir -p /etc/ssl/certs
  - mkdir -p /srv/gitea/nginx
  - chmod 660 /var/run/docker.sock 
  - chown root:docker /var/run/docker.sock

  # Clone Git Repository (use the PAT directly)
  - git clone https://oauth2:glpat-T8Taafup5xEd9x9a_PW4@gitlab.com/asif2fun/gitea-deployment.git /tmp/gitea-deployment

  # Copy certificates to the correct location (adjust if your repo structure is different)
  - cp -r /tmp/gitea-deployment/letsencrypt/* /etc/letsencrypt/
    
  # Pull Gitea Docker image BEFORE cloning repository
  # - docker login registry.gitlab.com -u asif2fun -p $GITLAB_PAT && echo "Docker login successful" || (echo "Docker login failed!" && exit 1)
  - docker login registry.gitlab.com -u asif2fun -p glpat-T8Taafup5xEd9x9a_PW4
  - docker pull registry.gitlab.com/asif2fun/gitea-deployment:latest # Ensure the latest image is pulled

  # Clean up temporary repository
  - rm -rf /tmp/gitea-deployment

  # Ensure proper permissions on the certificate directory
  - chown -R root:root /etc/letsencrypt
  - chown -R root:root /etc/letsencrypt/live/asifa.chas.dsnw.dev/
  - chmod -R 755 /etc/letsencrypt
  - chmod -R 700 /etc/letsencrypt/live/asifa.chas.dsnw.dev/

  # Generate Tokens and Secrets
  - INTERNAL_TOKEN=$(openssl rand -hex 32)
  - JWT_SECRET=$(openssl rand -hex 32)
  - LFS_JWT_SECRET=$(openssl rand -hex 32)
  - SECRET_KEY=$(openssl rand -hex 32)

  # Prepare Gitea Directories and Set Permissions
  - mkdir -p /srv/gitea/data
  - chown -R 100:102 /srv/gitea  # All directories and files under /srv/gitea
  - chown -R 1000:1000 /srv/gitea/nginx # Ensure Nginx user has write access

  # Prepare app.ini Configuration
  - |
    cat > /srv/gitea/custom/conf/app.ini <<EOF
    APP_NAME = Gitea: Git with a cup of tea
    RUN_USER = gitea # Make sure this is 'gitea'
    WORK_PATH = /var/lib/gitea
    RUN_MODE = prod
    

    [database]
    DB_TYPE  = mysql
    HOST     = mysql:3306
    NAME     = gitea
    USER     = gitea
    PASSWD   = gitea_password  # Replace with your actual password
    SCHEMA   =
    SSL_MODE = disable
    LOG_SQL  = true  # Enable SQL logging if needed  

    [repository]
    ROOT = /var/lib/gitea/data/gitea-repositories

    [server]
    DOMAIN                          = asifa.chas.dsnw.dev
    HTTP_PORT                       = 3000
    ROOT_URL                        = https://asifa.chas.dsnw.dev/
    APP_DATA_PATH                   = /var/lib/gitea/data
    DISABLE_SSH                     = false
    SSH_PORT                        = 22
    LFS_START_SERVER                = true
    LFS_JWT_SECRET                  = ${LFS_JWT_SECRET}  # Use generated secret
    OFFLINE_MODE                    = false
    CERT_FILE                       = /etc/ssl/certs/gitea.pem
    KEY_FILE                        = /etc/ssl/certs/gitea.key

    [lfs]
    PATH = /var/lib/gitea/data/lfs

    [mailer]
    ENABLED = false

    [service]
    REGISTER_EMAIL_CONFIRM              = false
    ENABLE_NOTIFY_MAIL                  = false
    DISABLE_REGISTRATION                = false
    ALLOW_ONLY_EXTERNAL_REGISTRATION    = false
    ENABLE_CAPTCHA                      = false
    REQUIRE_SIGNIN_VIEW                 = false
    DEFAULT_KEEP_EMAIL_PRIVATE          = false
    DEFAULT_ALLOW_CREATE_ORGANIZATION   = true
    DEFAULT_ENABLE_TIMETRACKING         = true
    NO_REPLY_ADDRESS                    = noreply.localhost

    [openid]
    ENABLE_OPENID_SIGNIN = true
    ENABLE_OPENID_SIGNUP = true

    [cron.update_checker]
    ENABLED = false

    [session]
    PROVIDER = file

    [log]
    MODE      = console
    LEVEL     = info
    ROOT_PATH = /var/lib/gitea/log

    [repository.pull-request]
    DEFAULT_MERGE_STYLE = merge

    [repository.signing]
    DEFAULT_TRUST_MODEL = committer

    [security]
    INSTALL_LOCK = true
    INTERNAL_TOKEN = ${INTERNAL_TOKEN}  # Use generated token
    PASSWORD_HASH_ALGO = pbkdf2

    [oauth2]
    JWT_SECRET = ${JWT_SECRET}  # Use generated token

    [app]
    SECRET_KEY = ${SECRET_KEY}  # Use generated token
    EOF

  # Ensure correct permissions on app.ini
  - chown -R 100:102 /srv/gitea/custom/conf/app.ini
  - chmod 660 /srv/gitea/custom/conf/app.ini

  # Create the Nginx configuration file
  - |
    cat <<'EOF'> /srv/gitea/nginx/nginx.conf
    events {
        worker_connections 1024;
    }

    http {
        server {
            listen 80;
            server_name asifa.chas.dsnw.dev;

            location / {
                return 301 https://$host$request_uri;
            }
        }

        server {
            listen 443 ssl;
            server_name asifa.chas.dsnw.dev;

            ssl_certificate /etc/ssl/certs/gitea.pem;
            ssl_certificate_key /etc/ssl/certs/gitea.key;

            location / {
                proxy_pass http://gitea:3000;  # Use the service name from docker-compose
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
            }
        }
    }
    EOF

  # Create docker-compose.yml
  - |
    cat > /srv/gitea/docker-compose.yml <<EOF
    version: "3.3"
    services:
      gitea:
        image: registry.gitlab.com/asif2fun/gitea-deployment:latest    #<your-gitlab-username>/gitea-deployment:latest
        container_name: gitea
        restart: always
        environment:
          - USER_UID=100
          - USER_GID=102
          - DB_TYPE=mysql
          - DB_HOST=mysql:3306
          - DB_NAME=gitea
          - DB_USER=gitea
          - DB_PASSWD=gitea_password
          - ROOT_URL=https://asifa.chas.dsnw.dev/  # Ensure ROOT_URL is HTTPS
          - INTERNAL_TOKEN=${INTERNAL_TOKEN}
          - JWT_SECRET=${JWT_SECRET}
          - LFS_JWT_SECRET=${LFS_JWT_SECRET}
          - SECRET_KEY=${SECRET_KEY}
        volumes:
          - gitea_data:/data
          - /etc/letsencrypt/live/asifa.chas.dsnw.dev/fullchain.pem:/etc/ssl/certs/gitea.pem:ro
          - /etc/letsencrypt/live/asifa.chas.dsnw.dev/privkey.pem:/etc/ssl/certs/gitea.key:ro
          - /srv/gitea/custom/conf/app.ini:/etc/gitea/app.ini:cached  # Mount custom app.ini
        networks:
          - gitea_network

      mysql:
        image: mysql:5.7
        container_name: gitea_mysql
        restart: always
        environment:
          - MYSQL_ROOT_PASSWORD=root_password
          - MYSQL_DATABASE=gitea
          - MYSQL_USER=gitea
          - MYSQL_PASSWORD=gitea_password
        volumes:
          - mysql_data:/var/lib/mysql
        networks:
          - gitea_network

      nginx:
        image: nginx:latest
        container_name: nginx  # Add a container name
        restart: always
        ports:
          - "80:80"
          - "443:443"
        volumes:
          - /etc/letsencrypt/live/asifa.chas.dsnw.dev/fullchain.pem:/etc/ssl/certs/gitea.pem:ro
          - /etc/letsencrypt/live/asifa.chas.dsnw.dev/privkey.pem:/etc/ssl/certs/gitea.key:ro
          - /srv/gitea/nginx/nginx.conf:/etc/nginx/nginx.conf:ro
        depends_on:
          - gitea
        networks:
          - gitea_network

    volumes:
      gitea_data:
      mysql_data:
    
    networks:
      gitea_network:

    EOF
    
  # Pull and Start Gitea and MySQL
  - docker-compose -f /srv/gitea/docker-compose.yml up -d
  
  
  # Fix Permissions Within Containers
  - docker exec gitea chown -R 100:102 /data/gitea     
  - docker exec gitea chown -R 100:102 /etc/gitea
  - docker exec nginx chown -R 101:101 /var/cache/nginx   
  - docker restart gitea                                 
  - docker restart nginx

  # Add a cron job to renew certificates (replace EMAIL and DOMAIN with your actual values, remove "--dry-run" for actual auto renewal)
  - echo "0 0,12 * * * root certbot renew --dry-run --quiet --standalone --preferred-challenges http-01 --post-hook \"systemctl reload nginx\" --email YOUR_EMAIL@example.com -d asifa.chas.dsnw.dev" >> /etc/crontab
