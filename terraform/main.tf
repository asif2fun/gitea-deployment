# main.tf

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.99.0"
    }
  }
}

provider "azurerm" {
  subscription_id = "82460ec9-3f27-4418-bf86-adc822a6bf8e"
  client_id       = "13f97e03-55d2-43b1-8522-d4cc0dfe1618"
  client_secret   = "7BB8Q~3lIhJw9enNXzOymMcJfz2.GvhJfDoNCaUJ"
  tenant_id       = "6800b1a8-d991-4b23-97f0-417c721ea803"

  features {}
}

# --- Resource Group ---
resource "azurerm_resource_group" "asif_gitea_rg" {
  name     = "asif-gitea-rg"
  location = "West Europe"

  lifecycle {
    prevent_destroy = true
  }
}

# --- Public IP ---
resource "azurerm_public_ip" "gitea_public_ip" {
  name                = "gitea-ip"
  location            = azurerm_resource_group.asif_gitea_rg.location
  resource_group_name = azurerm_resource_group.asif_gitea_rg.name
  allocation_method   = "Static"

  lifecycle {
    prevent_destroy = true
    ignore_changes  = all
  }
}


# --- Virtual Network ---
resource "azurerm_virtual_network" "asif_gitea_vnet" {
  name                = "asif-gitea-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.asif_gitea_rg.location
  resource_group_name = azurerm_resource_group.asif_gitea_rg.name
}

# --- Subnet ---
resource "azurerm_subnet" "asif_gitea_subnet" {
  name                 = "asif-gitea-subnet"
  resource_group_name  = azurerm_resource_group.asif_gitea_rg.name
  virtual_network_name = azurerm_virtual_network.asif_gitea_vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}


# --- Network Security Group ---
resource "azurerm_network_security_group" "asif_gitea_nsg" {
  name                = "asif-gitea-nsg"
  location            = azurerm_resource_group.asif_gitea_rg.location
  resource_group_name = azurerm_resource_group.asif_gitea_rg.name

  security_rule {
    name                       = "SSH"
    priority                   = 1000
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTP"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTPS"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "Gitea"
    priority                   = 1003
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3000"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# --- Network Interface ---
resource "azurerm_network_interface" "asif_gitea_nic" {
  name                = "gitea-nic"
  location            = azurerm_resource_group.asif_gitea_rg.location # Direct reference
  resource_group_name = azurerm_resource_group.asif_gitea_rg.name     # Direct reference

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.asif_gitea_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.gitea_public_ip.id # Direct reference
  }

  depends_on = [
    azurerm_virtual_network.asif_gitea_vnet,
    azurerm_subnet.asif_gitea_subnet
  ]
}

# --- Network Interface & NSG Association ---
resource "azurerm_network_interface_security_group_association" "gitea_nic_nsg" {
  network_interface_id      = azurerm_network_interface.asif_gitea_nic.id
  network_security_group_id = azurerm_network_security_group.asif_gitea_nsg.id
}

# --- Virtual Machine ---
resource "azurerm_linux_virtual_machine" "asif_gitea_vm" {
  name                            = "asif-gitea-vm"
  location                        = azurerm_resource_group.asif_gitea_rg.location # Direct reference
  resource_group_name             = azurerm_resource_group.asif_gitea_rg.name     # Direct reference
  size                            = "Standard_B1s"
  admin_username                  = "AzureAdmin"
  admin_password                  = "Asif@123"
  disable_password_authentication = false
  network_interface_ids           = [azurerm_network_interface.asif_gitea_nic.id]

  # Custom Data (Using templatefile)
  custom_data = filebase64("cloud-init.yml") # Correct the file extension

  os_disk {
    name                 = "gitea-os-disk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  depends_on = [
    azurerm_network_interface.asif_gitea_nic
  ]
}

# --- Outputs ---
output "gitea_vm_ip" {
  value = azurerm_public_ip.gitea_public_ip.ip_address # Direct reference
}
