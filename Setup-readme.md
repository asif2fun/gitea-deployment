# Gitea Setup by Asif Ahmed | VG

# This is a readme of how to Clone the gitlab repository and how to setup terraform  for gitea: 

    $ git clone https://oauth2:glpat-T8Taafup5xEd9x9a_PW4@gitlab.com/asif2fun/gitea-deployment.git

# After the cloning, navigate to the repository clone folder on your local machine 

# When the repository has been cloned navigate to the terraform folder and enter: 

    cd .\terraform\

# When inside the folder enter the following commands to start the infrastructure for the unattended

# Gitea setup:

    $ terraform init     # To create a Terraform configuration
    $ terraform apply 
            or 
    $ terraform apply -auto-approve # If you want to skip confirmation


# After the terraform has been applied wait 5 minutes for the whole Gitea setup to boot up,
# Then you can connect to the host machine via terminal with the login:

    IP: ssh AzureAdmin@108.142.225.39
    Password: Asif@123


# And check the docker container status by doing:

    $ sudo docker ps -a


# If the setup is complete you should see these values:
# It should be three container running the container gitea, gitea_mysql, nginx

# When the setup is finished you can access the site via this domain: 

    https://asifa.chas.dsnw.dev/

# You can then create a user and start using the gitea web-application.

# To test the applications functionality after reboot you can type the following command in the host:

    $ sudo reboot

# After reboot the setup should be working without a problem after a few minutes exactly as it should.

# To destroy the machine you have to use “terraform destroy -target option” because the terraform main.tf uses life-cycle on the resource group and public ip to prevent destruction of them and avoid public ip change. 

# Destroy machine process: 

# In your terminal type:

    terraform destroy `  -target azurerm_virtual_network.asif_gitea_vnet `  -target azurerm_subnet.asif_gitea_subnet `  -target azurerm_network_security_group.asif_gitea_nsg `  -target azurerm_network_interface.asif_gitea_nic `  -target azurerm_network_interface_security_group_association.gitea_nic_nsg `  -target azurerm_linux_virtual_machine.asif_gitea_vm -auto-approve


